﻿#--------------------------------
# I am at 882 out of 1807
#---------------------------------

#Scene w/Susa's mother marrying Akemiya.
#Scene w/Susa meets Soume.
#Scene w/Susa describes Mamoru as a child.
#Scene w/Soume figuring out Mamoru's diet.
#Scene w/Mamoru starts getting out of hand.
#Scene w/Mamoru bites Liza.
#Scene w/Susa trying to get it with Liza, finds her bitten.
#Scene w/Susa trying to kill Mamoru.
#Scene w/their son being "killed".
#Scene w/Soume being saved.
#Scene w/Susa-Liza break-up.

label su1:
    #play sound phone ringing
    su "Oi.  Who the hell is calling me this fucking late?"
    
    #play sound phone ringing
    su "Yeah, yeah.  Gimme a sec, here.  Hello?  Who is this?"
    l "Ah.  Susa.  I just wanted to give you an update on the situation.  Is this a bad time?"
    
    #Susa's voice gets soft here.
    su "Oh, Liza. If it's you, I can make time."
    
    l "Are you certain?  I can call again at another time."
    
    su "It's fine, I said. There was an ambush during a mission, stress is high right now."
    
    l "Ambush? Is Roman alright?"
    
    su "Yeah. Soume is taking care of him."
    
    l "Are you certain? I should come check on him..."
    
    su "Nah, Roman is fine. He's a tough kid. You don't need to come all the way down here."
    
    l "Ngh. Who? Who did this?"
    
    su "Do you need to ask?"
    
    l "Hmph.  Him.  I have been meaning to warn you, there has been a lot of movement by the Akemiya. I would suggest caution moving forward."
    
    su "Yeah, well, they are always doing something shady, aren't they?  A real pain in my ass."
    
    l "No. This is different, Susa.  I can assure you I have not seen them mobilizing like this before. Sneaking in is becoming harder and harder."
    
    su "Have you found out what they're up to?"
    
    l "It's only conjecture at the moment, unfortunately."  
    l "Some minor abduction plans, a few mentions of future targets, but nothing that would require the sort of movements I've observed."
    
    su "Hrm. Keep me updated."
    
    l "Certainly. I would suggest that you warn your students to keep up their guard when they are outside of the temple."
    
    l "The coming months may be a dangerous time for them."
    
    su "Ugh. Do they really need to know?"
    
    l "I believe it is in their best interests to."  
    l "I know some of the individuals inside that shrine personally...vigilance can not be counted amongst their strongest attributes."
    
    su "I know that.  I just don't want to make Kazutaka more paranoid than he already is.  Fuck, he's going to be in my office daily now if he hears about this."
    
    l "Doctor Osamu?  He has been trying to get in touch with me for a while now. You wouldn't happen to have any idea why?"
    
    su "Fuck if I know.  If I had to guess it would be some conspiracy to get him blah blah blah his old masters blah blah need to expel these three individuals from the shrine blah."
    
    l "I am not certain you should be taking his concerns this lightly."
    
    su "You don't know him like I do.  The guy is a genius, but he's also a bit nuts."  
    su "Every other month it seems like he has uncovered a new spy that we just gotta kick out or everyone's lives will be in danger."
    
    l "Heh.  Well, all the same, it might be best for me to return his calls."
    
    su "Be my guest.  If he has someone else to talk to perhaps he'll leave me alone."
    
    # Liza laughs.
    l "Susa, so chilly..."
    
    su "I'm being honest!"
    su "If I kicked out every person he thought was out to get him, it would only be me and him left, and I'm sure he'd start suspecting me as well."  
    su "He is still freaking out over the failed mission."
    
    l "Was he there?  I was under the impression he was a sensory demon."
    
    su "Yeah, but I sometimes send him on mission I expect to be simple."
    su "He's a right pain in the ass though, with those powers."
    su "'Oh, Miss Susa, I wouldn't eat that steak if I were you. I sense some sort of danger coming from it.  You better let me examine it.'"
    su "Yeah, right."
    
    l "Heh.  All the same though, it is rather odd that the group was ambushed if he was there with them."
    
    su "Well, he claims he couldn't sense the danger approaching.  Claims someone must have blocked his powers."  
    su "I would guess he just wasn't paying attention. "
    
    l "Hrm.  Interesting...I don't assume there is any news from your brother?"
    
    su "Why do you think there would be? We're not exactly on speaking terms."
    
    l "I merely wondered."
    
    su "..."
    
    su "Anyway. I assume you have some other...more pressing news."
    
    #Liza sounds painfully gentle, here.
    l "Not this time, Susa."
    
    su "Damn it."
    
    l "Don't give up hope. With all the moving they're doing, something is bound to drop."
    
    su "If only it were that easy to give up hope."
    
    l "I know..."
    
    l "You know I...I don't blame you anymo--"
    su "I'd rather not."
    
    l "Understood."
    
    l "..."
    su "..."
    
    l "Ahem. At any rate, it isn't just the Akemiya that are moving."
    l "The Kamiya are also making substantial moves, independently."
    
    #Susa has a dark grin here.
    su "Are they now? That's certainly unlike them."
    
    l "Indeed. They've brought in a few relatives from other countries as well..."
    l "There is a lot of to-do over a young lady, in particular."
    
    su "Sounds like they're preparing for a hostile takeover."
    
    l "That's what I am thinking."
    
    su "Heh. They never change."
    
    l "I plan on heading back to the Kamiya household tomorrow to find out more."
    
    su "..."
    l "...hm?"
    su "Perhaps you should take a break for a little while."
    
    l "What?!  No, that is completely out of the question."
    
    su "You said yourself how dangerous it was. If the Kamiya are moving that intensely..."
    l "You underestimate me, Susa."
    su "I'm just concerned."
    l "..."
    l "I appreciate the concern.  However, I can't.  Not now."
    l "We're too close."
    su "I...I know that.  That's why...that's why I can't lose you now."
    l "There is no need to worry.  I will take every precaution."
    su "If I remember correct, you're exceptionally loose on precautions..."
    l "Ha! Are you mistaking me for yourself?"
    su "I wasn't that bad!"
    l "You were awful. Good at what you did, but awful with authority. As you still are."
    su "...it's not that bad."
    l "Please don't worry about me. I won't get caught. Not when our son may still be alive."
    #Journal entry son go hurr
    return

label su2:
    "Of all the people I thought I'd ask for advice, she's last on the list."
    
    "Most of my day is spent trying to avoid her and whatever shit she is trying to hit me with."  
   
    "I just don't know who else to talk to about this."
    "She's a woman, right? So that's kind of like a mom. Sort of."
    $renpy.pause(1.0)
    "Roman is hurt, Soume doesn't like talking to things that don't bud, and I'd rather gargle glass than willingly go talk to Doctor Osamu about feelings."
    $renpy.pause(1.0)
    "I tried saying the words last night."
    "I killed someone."
    "With my bare hands."
    $renpy.pause(1.0)
    "I hate how I feel."
    
    "As I walk closer to Susa's room, I can hear her talking to someone."
    "Her voice is barely audible."
    "I probably shouldn't be listening to this..."
    "Eavesdropping on Susa's private conversations sounds like a good way to earn a few weeks' vacation on Doctor Osamu's hospital beds."
    $renpy.pause (1.5)
    "Fuck, this is a bad idea.  Maybe I should just head back to my-"
    
    su "I usually trust you, Liza. But I'm afraid you might be getting reckless."
    
    "That Liza woman again."
    "She's been calling a lot, lately."
    "From what Roman told me, Liza is compassionate, calm, and extremely helpful."  
    "That just makes her connection to Susa seem even weirder."
    
    su "I understand that. I just don't want you throwing your life away because--"
    su "Yes...well.  Well, I agree.  But I'm not going to let you do all of this alone."
    su "No, this isn't negotiable.  If you're doing this, I want to fucking help."
    
    "I move a bit closer."
    "What's going on?"
    $renpy.pause(1.5)
    "The conversation seems to lighten up."
    
    su "Right. Keep me updated." 
    su "Let me know when you're going in. I'll plan it from my end." 
    su "Thanks. I'll be awaiting further calls."
    su "Good-bye."
    
    "I hear her hang up the phone."
    "I probably should knock---"
    
    su "What do you want, Riku?"
    "Busted..."
    r "Uh...I was just...um...I-you see, I wanted to..."
    
    su "You can put your hand down anytime.  Doors open.  No need to knock."
    
    r "Huh?  Okay."
    #play sound door open
    r "I just want you to know that I did not hear anything important and I wasn't really even listening at all..."
    su "Shut up, idiot."
    r "Yes, ma'am!"
    su "Have a seat."
    r "Eh?  Oh...uh...okay."
    su "I'm going to get some tea.  You want anything?"
    
    r "No...I'm okay."
    
    "She leaves for the kitchen."
    
    "I've never really been in her room like this, before. It's kind of dirty. Video games cases fill up the bookshelves."
    
    "Maybe I should just look around?"

menu:
        "Look around the room.":
            jump looksuroom
        "No way, I'll get in trouble.":
            jump nolook
    
label looksuroom:        
    "Besides video game stuff, there isn't much else."  
    "No pictures of family or anything like that.  There is one large map on the wall next to her desk."  
    "Lots of blue dots.  There are pictures of kids up next to them, so maybe these are locations of Majin?"
    "There are also a couple of red push-pins inserted into a couple of different large cities."
    "Pictures of large, expensive looking mansions are next to those. I'm not sure what they're supposed to represent." 
    "Danger maybe."
    "And then there are a couple of green ones up towards the top of the map."  
    "There are no pictures near those, so I don't even want to guess what they stand for."
    "Hiding underneath a video game manual is a tiny slip of paper."
    "Looks like a photo. I pick it up."
    
    $unlock_item("memo", True)
    jump su2b
    
label nolook:
        "No way am I digging around her room. I want to live to see 20."
        jump su2b
        
label su2b:    
    su "Sorry 'bout that.  Can't focus on anything this late unless I have my tea."
    r "It's okay, I just---"
    su "You're torn up about what happened on the mission, right?"
    
    "See, I was right. She's just like a mom, underneath all that...other stuff."
    
    r "Yeah. I...I've never kiled anyone before."
    su "Mmm."
    $renpy.pause(1.0)
    su "I've killed a lot of people."
    
    r "Yeah?"
    su "A whole lot. So many I can't even count them, or remember their faces."
    
    r "Wow."
    r "How...how do you live with it?"
    
    su "It's pretty simple, really."
    su "What other choice do you have?"
    
    r "...huh..."
    
    su "You justify it however you have to. You're still here. You still have to live."
    su "So however you gotta work that, do it."
    
    r "Yeah. Yeah, but--"
    
    su "And look at the good you did. That guy was going to kill you and Kazutaka."
    su "You saved both of your lives."
    
    r "Yeah, that's true..."
    
    su "And you see what happened to Roman."
    
    r "Yeah..."
    
    su "You need to understand..."
    su "These aren't like the guys in movies and video games..."
    su "This is a war."
    su "They won't hold back because you're a kid."
    su "You can't use some crackpot, witty line to convince them to stop."

    r "I guess I did kind of think it would be like that. Like I'd just be a superhero and slay the bad guys and it'd be easy."
    $renpy.pause(1.0)
    su "I know that's exactly what you thought."
    su "I don't demand respect and obedience from you guys for fun."
    su "Well...mostly not for fun."
    
    r "That mission was far from fun."
    
    su "And some will be even less fun than that."
    su "But you fight on. If even one person can be saved because you were there...it becomes worth it."
    
    r "I don't know...I don't know anymore."
    
    su "Well,I didn't plan on putting you on anymore missions for a while, so don't shit yourself, okay?"
    
    r "Okay. Thanks for that...I...I'm going to train harder."
    
    su "Only what I've been tellin' ya since day one."
    
    r "Awlright, awlright!"
    r "Thanks, Miss Susa."
    
    su "My pleasure."
    
    "She's not so bad."
    $ decision = "15"
#menu:
        #"sleep"
        #"search"
        #"research"
        #"store"
        
label su3:
    pa "Susanoo. It is time to wake up. Laziness like this is an embarrassment to our family."
    "My full name is Kamiya Susanoo."
    "I was named by my father, after a male god."
    "I hate the name, always have."
    "I am the firstborn child of the head of the Kamiya clan, and the rightful heir."
    
    s "I'm coming..."
    pa "Shameful.  It is already half past four.  Animals are starting to rouse."  
    pa "If you are not ready by five, you can sleep with them tonight.  Perhaps they will improve your work ethic."
    
    "Real bastard, my father. More of a bastard than I'd ever want to associate with."
    $renpy.pause(2.0)
    "I stated formal training at the age of three."
    
    ma "Good morning, Susa, I've made you breakfast."
    "Breakfast is always meat. It's what we, Kamiya, eat."
    "Majin meat, freshly killed. Still raw and bleeding."
    $renpy.pause(1.5)
    "It tastes delicious."        
    "My mother would serve it every morning."
    
    pa "I hope this is the fine meat from the upper dungeon."
    ma "Of course, sir. Of course. I would never feed her anything less."
    pa "Good."
    
    "Looking back now, I wonder why I didn't realize what was going on earlier."
    "Maybe it's because I was taught that it wasn't an issue."
    
    ma "I was thinking, perhaps, darling, that we might stagger Susa's meals w-with other foods--"
    
    #play sound slam on table
    pa "Do not question me!"
    pa "My daughter will one day lead this whole clan! She will be the general of the Hunters!"
    pa "Her taste for blood must be ripe. There is no need to feed her slop."
    
    ma "Yes sir. Of course, sir. I apologize for mentioning it at all, sir."
    pa "Fine, good."
    pa "Go now, leave us. There is much for Susanoo and I to discuss privately."
    ma "Y-yes sir."
    

label su4:
    ##insert lines from Kazu path here, do not include portion where Kazu is talking to himself after his conversation with Susa, and use the following instead
    su "Well, fuck.  I got enough shit to worry about without Kazutaka going to pieces on me."
    su "Hmm...where was I.  Shit, I lost my place.  Somewhere around here, I think..."
    
    #play sound phone ringing
    su "Finally. Perfect fucking timing.  Hello?"
    l "Hello, Susa."
    su "Is something wrong?"
    l "No, of course not."
    #Liza chuckles. 
    l "There's no need to be so harrowed."
    
    su "Course not.  You're just running in and out of all the most dangerous houses in the country. Why should I waste my time being worried?"
    l "I appreciate the care."
    
    su "...nn."
    su "Yeah well...what did you find out?  Anything specific?"
    l "Well, Susa, it is a bit more--"
    su "Fuck.  Please just get to the point, Liza. I'm begging you. I haven't been able to concentrate all day. You were supposed to call hours ago."
    l "I apologize for the delay, but it's a bit more difficult than I imagined. I need to double-check the intel."
    su "That's fine, just tell me what you found out."
    l "I'll be returning to the Akemiya mansion tonight. If my intel is correct, the information we need is there."
    su "Tonight?  Wait for me, I'll-"
    l "No. I cannot wait for you to arrive. I must go within the hour."
    su "Can you trust your intel?"
    l "It has never disappointed me before. I urge you to stop worrying. This is not the first time I've been inside the Akemiya mansion, and it is doubtful that it will be the last."
    su "Well, fuck, that is reassuring.  I mean, if you keep breaking into the same location over and over, surely that DECREASES the chances you'll end up caught."
    l "Susa."
    su "Fine. Sorry."
    l "I won't fail."
    su "...right."
    l "Well, then, I'll-"
    su "And then...then maybe we can go back.  To the ways things were before.  It was...losing him that caused all of this."
    l "Yes, well...that is something to discuss another time.  Right now, I think our energies are best focused on the task at hand."
    su "R-right.  Of course.  Luck, Liza."
    l "I shall not need it.  But thanks."
    
label su5: 
    db "ACHOO!"
    db "AH!  Dammit, what did I tell you about sneezing.  Were 'sposed to be quiet out here."
    db "I can't help it, I have a cold."
    db "Well, all I'm saying is you need to stop yourself sneezing." 
    db "How are we supposed to be standing guard out here all sneaky like if you're letting everyone know you're here with your sneezes.  It ruins our cover, doesn't it?"
    db "What do you want me to do?  I can't help it."
    db "Just...I dunno...hold your breath.  Or something.  Sneeze inside yourself."
    db "That...might be the dumbest thing you've ever said.  How can I-"
    db "ACHOO!"
    db "Now you're just doing it on purpose."
    db "Am not!  I can't stop myself from sneezing."
    db "Figure something out.  You're letting everyone know we're here."
    db "You're letting everyone know we're here with your fucking yelling.  Just shaddup and stand guard."
    db "How can I stand guard when you're sneezing.  It is distracting.  What if someone snuck up and killed us in the middle of one of your sneezes?"
    db "In the middle of my sneeze?  And how the fuck would they do that?"
    db "Well...I dunno.  You close your eyes when you sneeze, don't you?  Let your guard down, when you do that.  Bad form."
    db "Everyone closes their eyes when they sneeze!  It is a reflex."
    db "I don't."
    db "Yes you do, you moron."
    db "No I don't.  I trained myself.  Everyone knows you're most vulnerable when you sneeze."
    db "Oh, please.  What about when you sleep?  Isn't that when you're the most vulnerable."
    db "You would think so, but that ain't what the statistics show, is it?  Second most vulnerable, they say.  And I don't close my eyes then, neither."
    db "The fuck you don't.  You're just making all this up right now."
    db "..."
    "..."
    l "Hmph.  Almost too easy.  The requirements for guards have clearly gotten more lax, recently."
    l "Let me see...here."
    l "Hmm...I need to get here without being seen. Guard detail is low tonight, but I still must be cautious.  One wrong move and I'm dead."
    db "I've been meaning to ask, does this look like a rash to you?"
    db "Put your fucking shirt back on, ya moron."
    l "...perhaps now is the best time to leave."
    
    ###minigame here.  Liza sneaking game.  Get past the guards without being seen, or you must start over.
    #play sound phone ringing
label su5a:    
    su "Liza, is-"
    l "We should meet. Soon."
    su "Is he alive, Liza?  Please tell me he is alive."
    l "I do not have time to discuss this in detail.  We need to-"
    su "Liza, don't fucking play games! I have a right to know."
    l "...he is."
    su "..."
    su "Oh god..."
    l "Compose yourself, Susa. This line may not be secure. When can we meet?"
    su "Just--"
    l "I repeat myself:  when can we meet?"
    su "Roman's birthday party. You know what day it is."
    l "Perfect. I will see you then."
    su "I'll talk to you--"
    #play sound dial tone
    su "..."
    
label su6:
    db "And then I fuckin' have him hanging by the ankles, eh?  Just fuckin' dangling there."
    
    db "Heh, I must admit, you have true talent."
    
    db "Fuckin' right I do.  Anyways, the stupid sap is fuckin' crying, right?"  
    
    db "And I'm tryin' to drain him for the boss, but the last time I did this he fuckin' was all grumpy because the tears made the blood too salty."
    db "Perhaps you should just kill them before you drain them, like the rest of us."
    
    db "Well, I ain't used to this yet.  Most of the time he just fuckin' eats them, doesn't he?  I've only had to drain them twice now."
    
    db "It is a bit difficult, but I find the process goes much quicker if you kill them before the process begins."
    
    db "Oh, now where is the fuckin' fun in that, eh? Who's gonna keep me company while I'm workin'?"  
    db "We had a good conversation, though. He was cryin' about his wife or somethin'. Think we might wanna pay her a visit, eh?"
    
    db "She a Majin though?"
    
    db "Didn't say. Anyway, so he's fuckin' crying, and I can't get him to stop.  So, I pull out my blade, and I say--"
    l "Enough."
    db "'Scuse me?  Did ya say somethin' to me?"
    l "I said enough. Professionals feel no need to discuss their craft as you do."
    db "Well, fuck, darlin'.  Maybe you should shut up and mind your own business."
    l "And perhaps you could finish evolving."
    db "The fuck you say to me, woman?"
    
    db "Easy. This one's half oni."
    
    db "Shaddup. I'm not takin' shit from no bitch."
    db "Where the fuck do you get off judgin' me?  You fuckin' Nazi."
    l "..."
    db "Didn't think we heard about that, eh?  News gets around, you fuckin' high and mighty prick."
    l "I would drop that subject, if I were you."
    db "Oh, looks like I touched a fuckin' nerve.  What's the matter?  You get tired of huntin' one group so you move on to the next?"
    #play sounds of getting beat up.
    db "MY FUCKIN' NOSE!"
    db "WHAT THE FUCK IS YOUR PROBLEM, YOU-"
    
    su "Enough."
    db "Oh...General. I uh...I didn't see you...you know...there..."
    
    su "I'm sure."
    su "You there, new tall person. Did you do this?"
    
    l "I did."
    
    #Susa chuckles.
    su "What's your name?"
    
    l "Elizaveta Sagodi."
    su "Ohhh, that's right. I heard about you. Transplant from Hitler's own personal army."
    
    l "That is correct, General."
    
    su "Obedient and prompt. I like that." 
    su "You're used to military, aren't you? Germany trains theirs well."
    l "Yes General, they do."
    
    su "Heh. Alright. Let's see what you can do, then. You're the second-in-command on this mission."
    
    l "Thank you, General. I appreciate the opportunity to show my skill, General."
    
    su "And as for you..."
    
    db "Y-yes, ma'am?"
    
    #sound of a single blow and a person dying
    
    su "You're off the mission. Can't have your the smell of your blood giving us away."
    l "..."
    su "Everyone move out!"
   
label su7:
    "When I was twenty years of age, my little brother was born."
    "I'd never had to share anything in my life."
    "And I hadn't planned to start."
    
    #play sound door opening
    su "Mom?  I came as soon as I-"
    #play sound crying
    su "--ah--"
    
    "He was premature, and so so small."
    
    pa "Quiet the thing. With strangulation, if need be."
    ma "He is just a child.  He doesn't know any better."
    pa "QUIET HIM, I SAY."
    
    "I backed away. When Father yelled, only bad things were to come."
    
    pa "Embarassing. Pathetic. Do your remember Susanoo ever carrying on like this? He is weak."
    ma "He is not weak! He--he's just an infant, he needs time..."
    pa "Susanoo did not need any TIME, because she is truly my daughter."
    pa "You've been around, haven't you, you harlot?"
    ma "Of course not, sir, never--"
    pa "Are you SAYING then, that this weakling is of my stock?"
    ma "I--I am only saying that he is too little---"
    pa "Do not think that worthless urchin has anything to do with me. You're lucky I don't drown him."
    ma "Please--let me take care of him--"
    
    #play sound smack
    pa "Do not presume as if your opinion matters! If I tell you to drown him, you DROWN him."
    ma "Please!!"
    #play sound thud
    #play sound crying
    
    pa "If your son will not stop, I will be forced to silence him using my own methods."
    ma "Please--please don't--"
    
    "I can't remember what I was thinking, then, watching this go down, with a blank look on my face."
    
    pa "I know what we shall do. Susanoo. Come here."
    su "Yes, Father?"
    
    pa "My precious daughter. Look here, at this boy. You shall decide his fate."
    pa "If he is worthless, then we shall drown him."
    pa "But if you find him worthy of life, he shall be allowed to live."
    
    "I peer more closely. He was so small. So weak. It would have been easy to crush his skull and be the precious only child, the prodigy."
    "I wish I had killed him, then."
    
    su "Let him live. Who knows what he may be useful for later."
    pa "And that is your final decision?"
    #stick choice here for no reason
    
    pa "Then it is as you wish, my love."
    pa "You're lucky. My daughter has decided he live. What is his name?"
    
    ma "I think...I think his name is Mamoru."
    
    pa "Feh."
    
    "Mamoru. I liked that name. It was cute, like him."
    "Hindsight is always 20/20."

label su8:

    "I was technically the General of the Kamiya Hunters."
    "I hated it."
    "But it was an excuse to kill, and I was a born and bred killer."
    "I think that evening...we were just biding our time until morning light."
    "Curled around a fire, chattering and making food and eating."
    "That had been my life." 
    "It was all I knew."
    
    su "New recruit--uh--Elizaveta, right?"
    l "Liza is fine, General."
    su "What're you doing sitting all the way over here?"
    l "I must remember my place at all times."
    
    "It was such a curious answer." 
    "She was a hunter, like us, even though she was Majin."
    "Sure, we'd rib each other by threatening to eat each other, but none of us were serious."
    
    su "You're the second in command. The Captain. One of the best damn fighters we have."
    su "That's your place."
    su "Come sit at the fire with us. There's other kinds of meat."
    
    "Besides the meat of her brother Majin, I meant."
    "I was blind."
    
    l "If you please, General, I am far from worthy."
    
    "That bothered me. Here I was, praising her efforts, offering her a seat at my side, and she was refusing it."
    
    su "I'm TELLING you that you're worthy."
    
    "She still said no."
    "She said no every time."
    
    "So once, I just went over there to sit with her."
    
label su8b:
     su "So now I'm sitting here with you, okay? In the dark, away from the food."
     l "It's highly unnecessary, General. I urge you to enjoy the merry-making with your comrades."
     su "Why are you like that?"
     su "I keep trying to include you, cause we're all a team, but you keep turning me down."
     su "What if I ORDERED you to come sit with us?"
     l "Then, as all orders, I will follow them."
     su "Why do I have to order you to enjoy yourself?!"
     l "..."
     l "General, I did not realize how thoughtful you were."
     
     "Why did she say that? I was far from thoughtful, far from kind."
     "I murdered children in their sleep and enjoyed the taste of their blood."
     "Being this way was something I merely accepted. Something I had learned to enjoy."
     
     su "I---that's---I'm not like that!"
     l "Why do you say that? It's a compliment."
     su "I'm not some goody-goody!"
     l "Neither am I."
     l "But that's not necessary to be thoughtful."
     su "It's not?"
     l "Not at all."
     su "..."
     su "...you're weird. It must be that European thing. Most Majin I know just slobber all over themselves and piss their pants."
     l "..."
     l "Most everyone I know would do that, General, if they saw you approaching."
     su "Yeah, I'm pretty scary."
     l "Mmm. I don't find you to be so."
     su "What? I'm fucking scary. I could bite your head off right now!"
     l "I'm sure you could. That still doesn't frighten me."
     su "Well you're just a freak."
     l "..."
     l "Why is it important for you to be frightening, anyway?"
     su "I don't know. Fear makes people respect you."
     l "Fear only makes people fear you. That is not the same as respect."
     su "It's basically the same thing."
     l "Fear makes people obey you."
     l "Respect makes people die for you."
     
     "Everything she said bothered me. Every bit of knowledge I asserted, she had a better answer for."
     "She didn't respond differently whether I was angry or calm."
     "I wondered how a Majin like that could even exist."
     
     "I wanted...to learn more."
    
label su9:
    rb "S-spare me! You're a Majin, too, right? Just let me--"
    l "I must follow orders without question."
    rb "But...but you're like us!  Please, just let me go!  I can offer you-"
    l "Nothing you have could possibly interest me. I cannot help you."
    rb "But-"
    l "Please, take her away.  I need to check on the others and ensure they were able to round up the stragglers."
    db "Yes ma'am.  This way, maggot."
    
    #play sound thud
    
    rg "Ngh.  Pl-please. H-help."
    
    #play sound thud
    l "Quiet. At least go with dignity."
    
    "They always begged, without fail."
    "They saw me, their sister, coming, and always appealed to me to save them."
    "They never realized they were already too late."
    
    db "...Cap...tain..."
    l "What happened? Where are the others?"
    db "They--one of them is--strong..."
    db "..."
    l "No time to go back and get reinforcements--damnit."
    
    "..."
    
    #play sound rustling leaves
    l "Who's there? Show yourself!"
    
    rg "Go. Leave here and never return, or meet the same fate as the humans."
    l "..."
    
    "I can't leave without the full lot either enslaved or dead. I know that much."
    
    rg "I said leave, Sister. I will kill you if I have to. I know what happens to those who are captured."
    l "..."
    l "I cannot."
    rg "Then, traitor to your own people, I will end your life here."

    "Traitor. I had heard that word so many times."
    "The blow it gave never lessened."
    
    rg "This is your last chance."
    
    "I prepare myself. In my mind, it was better to live and be a slave than die and lose all chance of freedom."
    "Not everyone I faced agreed."
    
    l "I cannot."
    
    rg "THEN DIE! HAAAAAAAA!"
    
    #insert fight scene.  Liza versus two Majin."
    
    l "You fought well."
    
    
    "I disliked killing."
    "But in this world, it was either me, or them."
    "My life was comfortable."
    "I didn't sleep on a stone floor, with chains around my ankles and wrists."
    "I was rarely bothered by humans."
    "Rarely threatened."
    "Privileges that others like me paid for with their lives."
    
    #play sound crying
    
    l "Ah--"
    
    "No. It can't be."
    
    l "This is what she were hiding. ...this wasn't in the mission notes."
    
    #play sound crying
    
    "Hidden behind the dirty wallboards was a baby...a little boy."
    "He quieted when I picked him up, looking at me with large, alert eyes."
    
    $renpy.pause(2.0)
    "That little boy changed me."
    
    db "Captain, are you alright?"
    l "Yes---"
    
    "Damn. I can't let them have him. If nothing else, not him."
    l "I--Yes, stay where you are! I will come up."
    db "Glad to hear it. I saw the bodies back there, and I didn't know what happened."
    l "Yes, two of them defended themselves, but they've been disposed of. I'm checking the perimeter for stragglers."
    db "Understood. What are your orders?"
    l "Take the dead bodies back to the base. They may still be of use."
    db "Understood."
    l "Excellent. I will follow once I have completed my check and reported findings to the General."
    db "We will report back to you once we have returned as well."
    l "Good.  Dismissed."
    
    #play sound footsteps
    l "...you've gotten me into trouble, you."
    
    "He seems to be the cleanest thing in the entire area." 
    "His mother loved him fiercely, that much is clear."
    
    l "...what to do with you?"
    
    "He grabs onto my finger."
    "I can't take him in."
    "I know what they do to children."
    
    #play sound baby noises
    
    su "Liza, are you--what is that?"
    
    l "Shh--it's...the woman hiding here had a child."
    
    su "Excellent! We'll have an extra surprise to bring back--"
    l "No...not...not this one."
    su "...what?"
    l "A mother gave her life for this child. I can't--"
    su "..."
    su "If you were anyone else, I'd cut you down where you stand, you know that?"
    l "..."
    $renpy.pause(2.0)
    su "..."
    su "Alright. Just--keep that thing hidden. Shit."
    l "Susa."
    su "What?"
    l "You are a kinder human being than you give yourself credit for."
    su "Don't sling insults just cause I letcha keep it. I'm not going to stick up for you if you get caught."
    
    "I know that's a lie."
    
    l "We're just going to get you nursed back to health. It looks as if you've had a bit of a rough go, hasn't it?"
    su "..."
    l "What is it?"
    su "So what're you going to name it?"
    l "Mmmm. Susa, do you like children?"
    su "N--no! I hate EVERYTHING. What a STUPID question."
    
    #Liza chuckles.
    l "Ahahahaha!"
    
    su "...that's not funny."
    
    l "What do you think we should name him?"
    su "I don't know. I'm not good at this stuff."
    l "Mmm. How about Oskar?"
    su "I GUESS that's okay. If you like that kind of name."
    su "Which I don't."
    l "Hehehe. Oskar it is."
    
   
label su10:
    m "Ha ha ha! Yum!"
    ma "Yes, what a good boy. Eat up all of it. You must grow up big and strong~"
    su "The kid eats like a fuckin' starved hyena. And Liza is right here--"
    l "Do not worry about it, Susa.  I...don't even notice."
    
    pa "Very good. Someone who understands her place in the world."
    su "She's MY guest. You could be polite."
    
    m "Yum yum yummy yummy."
    su "What're you feeding him? It smells rank."
    
    ma "Strained vegetables with bits of raw meat."
    
    pa "No true child of mine would eat such garbage."

    l "..."
    
    "It didn't occur to me, for a long time, that Liza might not enjoy eating dinner with the family."
    "We were, after all, eating her kind."
    
    "What should I do?"
    
menu:
        "Eat somewhere else.":
                jump su10a
        "Deal with dinner.":
                jump su10b
                
label su10a:        
    su "We're going to take our dinner out of here."
    pa "Good evening, then, Susanoo. And Susanoo's captain."
    
    "He never remembered her name, not one time."
    
    ma "What a good boy.  Yes, eat up every bite."
    m "Hee heeeee!"

label su10b:

    "We'll stay, I guess."
    #--NEED TO ADD TO SCENE HERE--
    
label su11:
    su "Aw c'mon...eat.  Just...eat kid.  Cram it down yer' little gullet.  No, no, no!  Fuck.  Don't spit it out."
    l "Liza, no not like that.  You can't just shove the spoon in his mouth!  Be more gentle."
    l "Like...this.  There you go.  Awww...look at you Oskar.  Mmmm.  That is good isn't it?  Yum."
    su "Well, look at you.  When did you suddenly become so good with kids?"
    l "Oh, Susanoo, it really isn't that difficult.  You just need to learn to be more gentle with them.  You cannot talk to children the same way you talk to talk to the soldiers."
    su "Hrm.  I suppose that makes sense.  Sort of."
    l "Mmm.  Yes, very good!  Yummy!  Come on, make sure you get all of it.  Here Susanoo, why don't you try."
    su "Er...hey kid.  Eat this food.  It tastes good and it is good for ya."
    l "...that does sound like a nice marketing slogan.  However, I worry about its efficacy on children not yet old enough to understand the words."
    su "Ha.  I just need some more practice is all.  Maybe I'll just watch you some more."
    l "Fair enough.  You do need to become more familiar with these sort of things, now that you have a baby brother and a son."
    su "Ugh.  Don't remind me about that little brat.  Fucker almost bit my hand again the other day."
    l "Hmm...he does have a hungry look in his eyes.  There is something about him that seems a bit odd."
    su "I've noticed it too.  First I just thought it was my weird as family and how they're raising him.  But there is definitely something about him."
    l "He seems fairly harmless, for now at least.  I have a hard time imagining him crawling around and attacking people."
    su "Yeah, well, for now he's harmless.  Kind of.  He isn't just crawling, any more ya know.  He is walking already.  You should see him around Majin though."
    l "I'd...rather not."
    su "Er...right.  But they've started giving him live food.  Well, Iza hasn't.  He doesn't really know about it yet."
    su "But...mother has been giving him live food.  Children.  The way he devours them...it is a bit frightening."
    l "How so?"
    su "Well...he is rather...I dunno.  Sadistic about it?  At first, he would start chewing at parts that would kill them quickly.  The neck and what not."
    su "Recently...he has been taking his time.  Savoring their pain, it almost looks like.  He started talking recently.  He has already learned how to taunt them."
    l "Hmm...well, I don't think there is much you can do about it.  Oh, Oskar.  Stop fussing.  Fine, fine, you can get down.  Just careful where you're running."
    su "Well...there is something I can do."
    l "And what is that?  Oskar, get down off the sofa!  No, now!  I don't want you putting any more holes in our ceiling!"
    su "I was thinking...about...taking care of the situation.  Before it comes one."
    l "Huh?  What do you mean taking care of it?"
    su "Y'know.  Taking care of it?"
    l "...?"
    su "Fuck.  I was thinking about kiling him.  Damn, really don't like the way that sounds out loud."
    l "Susanoo!  You cannot be serious!  He is your brother.  Your mother would be devestated."
    su "I know, I know.  But I know there is something dangerous about him."  
    su "If we let him keep growing at this rate, he will be a terror to Majin everywhere.  I've never seen anything like him before."
    l "Perhaps...we could turn him?  He is your brother after all.  You were raised in the same environment, and you turned out wonderfully."
    su "Thanks.  Appreciate it.  But there is something off about this kid.  I can see it in his eyes."
    l "Just tell your father.  If he knew how often your brother was being fed, he'd stop Bachiko.  That should modulate his strength until we have time to think of something."
    su "Yeah, well, I don't like talking to that old bastard.  He's a fuckin' prick, and I'd rather not have to ask him for help.  Besides...I think he's scared of the kid."
    l "Huh?  Scared?"
    su "Last week he hit my mother.  The kid woke up when he heard the crying."  
    su "They...don't really know what happened next.  My father won't tell me, and my mother just smiles when I ask."
    su "But...the kid somehow broke my father's arm.  He was in bad shape next morning."  
    su "Burn marks all along his face, too.  A real fuckin' mess.  I'd have celebrated if it wasn't for the fact it was the little shit who did it."
    l "That...that can't be.  Your brother isn't even three yet.  He's still just a baby.  They...they don't have that kind of power."
    su "Wut I thought, too.  But, trust me, my mother didn't do that to him, and neither did I.  Had to have been the fuckin' kid.  No other explanation."
    l "...oh my.  Could you...talk to your mother."
    su "Her?  She's fuckin' nuts.  Living with Iza that long would probably do that to anyone.  Still though, she's proud of the little shit.  That's why she keeps on feeding him."
    l "But surely she would understand the danger he poses."
    su "Not to her.  The brat fuckin' loves her.  Doesn't even look at anyone else."  
    su "First time in my mom's whole life she has got a taste of some power.  Didn't think she'd react this way, though.  Can't say I blame her."
    l "Then...then...I don't know what to do, Susanoo."
    su "Well, like I said, that's why I have to take care of it.  I can't let him reach adulthood.  He'd be a real fuckin' menace.  Best to take care of this now."
    l "...oh Susanoo.  I will support you in whatever you decide.  But I urge you to reconsider.  There have to be better ways to deal with this that we just haven't thought of yet."
    su "I don't think so, Liza.  Once I work up the courage, I'm gonna sneak in there and kill him in his sleep.  Peacefully."
    l "That...is horrible.  Even if he is violent now, that does not indicate what he will be like for the rest of his life.  He isn't some hopeless case."
    su "Fuck.  What d'ya know, anyway?  You haven't been in there alone with him."
    l "...that is because your parent will not let me in the house anymore.  Something about how my kind befouls their home."
    su "...right.  Look, ya know I dont fuckin' buy that shit."
    l "Hmm...yes, I do believe that.  But just because I am not intimately familiar with your family does not mean I do not have a basic sense of justice and what is right."
    su "Hmph."
    l "I beg you to reconsider your plan, Susanoo.  We can resolve issues in ways that do not rely on violence.  Do not forget this."
    su "But violence has always worked so well in the past.  Kinda fond of it."
    l "I can tell.  But perhaps it is time we started moving past that.  That is what they expect out of me.  Since I am so 'wild' and 'untamed' and however else it is that they describe me."
    l "Perhaps I have been playing into their preconceptions all this time.  You too.  But...it doesn't have to be this way."
    su "Look. let me take care of this kid, my way.  Next time, we can try your method of...whatever the hell it is you'd wanna try."
    l "...I see.  If you excuse me, I need to check on our son.  I'll leave you to your plotting."
    su "Hmph.  Well, she's in a sour fuckin' mood."
    su "She'll come around.  I know I'm right about this."
    su "Sorry brother.  This house is simply not big enough to accomodate the two of us."

label su12:
    
    #play sound footsteps"
    
    su "(Ngh.  Need to be more quiet than this.  Mum always barely sleeps anyway.)"
    
    #play sound creak"
    su "(FUUUUUUCK.  Stupid board.  Told Iza he should have fixed this weeks ago.  How the fuck am I supposed to sneak in here if this damn board is giving away my position?)"
    su "(Hmm...didn't hear anything though.  I guess I might be-)"
    db "Susanoo?  Is that you?  What are you doing here so late?"
    su "(Fuck fuck double fuck.  So much for my stealth skills.  Liza was always better at sneaking in places.  Probably should of asked for some tips.)"
    su "(Not that she'd really help me with this.)"
    su "Uh...yeah, ma.  Just me.  Came in for some food, is all."
    db "Ah.  Is Liza not feeding you properly?  I can have a word with her, if you so desire.  Your brother left some of his leftovers in the fridge, if you are hungry."
    su "She isn't just some servant, ma.  You know that."
    db "Ah.  Yes.  You two are...in a relationship.  Of course.  But she should still be taking care of you.  She is property of the master, after all."
    su "Ngh.  Could you please not talk about her that way.  It makes things sound all weird."
    db "If you desire, Susanoo.  But just be aware, when you finally grow out of this phase you are passing through-"
    su "It isn't just some fuckin' stage!  I'm not dating Majin just for kicks or to piss off Iza."
    db "Please lower your voice.  My son is trying to sleep, and I would hate it if you woke him."
    su "Ngh.  Sure.  Whatever you say."
    db "And of course this is just a phase.  You see Majin as something exotic.  Something different and fun.  You'll outgrow this.  You'll see them for what they are."
    su "You aren't fuckin' listening to me.  At all.  I'm trying to tell you-"
    db "Yawn.  I really should be getting back to sleep.  Oh, did I tell you?  Your brother killed his first adult Majin today!  All by himself too!  You should have seen him.  You would have been so proud."
    su "...I'm sure I would have been.  Why don't you get back to sleep.  Long day ahead of you, I'm sure."
    db "Oh, of course.  Your brother is such a handful sometimes.  But you can't just stop yourself from loving him.  What a perfect specimen he is.  He shall be the envy of our whole clan."
    su "Right.  Sure.  'Night."
    db "Good night, Susanoo.  Sleep well.  Tell your Majin friend to try not to track mud all over our yard the next time she stumbles over."
    su "...right."
    su "(That was bullshit, but I'm not going to call her on it right now.  Liza is the most clean person I know."  
    su "Those mud tracks throughout the yard come all the way into the house, and she has to know by now they are mine.)"
    su "(No time for that now though.  I need her to get back to sleep soon.  I need to sneak into my brother's room.  Tonight.  I can't back down.)"
    su "(I'll just wait a couple of minutes to make sure she is fully asleep.  I do not want to have to fight her off of me in the middle of this.)"
    su "(Hm.  Look at all these pictures she has up.  Grotesque.  About half of them he is covered in blood, and smiling a shit eating grin right at the fucking camera.)"
    su "(Ugh.  These are disgusting.  Was I ever like this?  No.  No, I couldn't have been.  I was never this cruel.  It...it was just for food.  Not pleasure.)"
    su "(They used to have some pictures of me up here.  Now they're all taken down."  
    su "Iza probably did that sometime after he laid into me about dating Liza.  I know for a fact he's raped dozens of Majin women, the fucking savage hypocrite."
    su "(Tried to bring that up to him, and he told me that what I was doing was different.  Caring for her.  Not just using her for pleasure.  'A partnership,' he sneered at me."
    su "(I fucking like it better this way.  The less he talks to me the easier it is to pretend we are not related.)"
    su "(I can hear snoring coming out of ma's room now.  I think I should be good.)"
    
    #play sound door opening"
    
    su "(There he is.  Sleeping on his bed.  Even when he sleeps he is baring his teeth.  A little disturbing, if I'm being honest.)"
    su "(...what am I going to do now?  How should I even do this?  I haven't planned this part out yet, obviously.)"
    su "(Should I...just use my powers?  Perhaps that would be the easier way.  It might make some noise...if mother finds out this was me, she'll never talk to me again.)"
    su "(Though, if I stay together with Liza much longer, I doubt that would really change how she treats me anyway.)"
    su "(I don't want to hurt the brat.  Just need to kill him.  What would the most painless way be, I wonder?)"
    m "...sister?"
    su "(FUCK!  Scared the shit out of me.)"
    su "(Again, my stealth skills appear to be a bit rusty.  Woke up ma, and now the kid.  Fucking fantastic.  Maybe I can wake up Iza and go for the whole family.)"
    su "...uh, hey kid."
    m "Brother.  I am your brother."
    su "...right."
    su "(Fuck, don't call yourself that.  It is going to make this whole thing worse.)"
    m "Is...is mommy alright?"
    su "Huh?  Oh, yeah.  She's fine.  Just saw her."
    m "Good.  Why are you here, sister?"
    su "(Fuck, this kid is smart.  Didn't know he was already talking in full sentences.  That puts him ahead of Oskar, and if I had to guess Oskar is a bit older.)"
    su "Just...checking in.  Seeing how everyone was."
    m "Oh.  Are you worried about mommy, too?"
    su "Huh?"
    m "That bad man hurts mommy.  A lot.  I hear mommy crying."
    su "Yeah...he's...well, he's an asssss......er....yeah, he's a bad man."
    m "Yes.  Yes, he is.  The other day, I stopped him though.  I made him cry."
    su "Right...I saw that."
    m "I need to get stronger."
    su "Huh?"
    m "Mommy said if I eat more, I'll grow stronger.  Is that true, sister?"
    su "Yeah, I suppose.  We do get strength from our food."
    m "Good...I'm eating as much as I can now.  I need to get strong.  For mommy."
    su "(So that is why the little brat is going through Majin like a fucking hyena.)"
    m "...can you help me, sister?"
    su "Huh?  I'm not going to get you some food right now, kid.  You'll have to get ma to help you with that in the morning."
    su "(If you even have a morning.)"
    m "No.  That isn't what I want."
    su "...then what do you want, kid?  Dunno what I can do for ya."
    m "...I need help protecting mommy.  I...I'm not sure if I can do it on my own."
    su "Oh.  Yeah, I see what you're saying."
    su "(Kind of a sweet sentiment.  No wonder ma loves this kid.)"
    m "You are very strong.  I hear mommy and the bad man talk about you sometimes.  Very strong.  You can help me, right?"
    su "I...I don't know..."
    m "Please.  Please sister.  I...I'm afraid mommy might get hurt.  I don't know if I can stop the bad man anymore."
    su "Huh?  You did a good job the other day.  Roughed him up pretty daaaaa-uh...dang good."
    m "But...that was an accident.  I don't know how to do that again.  I've tried.  I...I don't want mommy to get hurt again."
    su "(Fuck.  So even he doesn't know how he used his powers.  Just looks like a scared kid right now.)"
    su "Uh...sure, I'll make sure he doesn't hurt her.  I'll try my best at least."
    m "...thank you sister.  Thank you so much."
    su "(I sit next to him for the next half hour or so.  He wants me to tell him stories.  I tell him a bit about what I do.  About al the missions I go out on.)"
    su "(I leave out the bloody stuff though.  At least I try to.  Some stuff slips.  It seems like his favorite part.)"
    su "(Eventually, he falls asleep.  He is lying there, helpless before me.)"
    su "(And I leave the room.  I can't bring myself to do it.  I can't believe what I almost did.  This kid isn't a real danger to anyone.)"
    su "(He's just afraid of Iza.  Can't blame him, honestly.  Liza was right.  Fuck, Liza was right.  Don't want to admit that to her.  But she was right, there is hope for this kid.)"
    su "(I just need to make sure I'm around to lead him in the right direction.  Make sure he doesn't take after Iza.)"
    su "(...and I'm sure Liza won't mind helping.)"
    
label su13:
    l "Oskar, be careful around Susa's brother!  You are playing far too rough!"
    m "He he he!  He is fine!  This is fun!"
    su "See?  They're fuckin' fine, Liza.  Relax."
    l "Susanoo!  Language!  It is a small wonder that Oskar and your brother have yet to start having the vocabulary of drunken sailors."
    su "Aw c'mon, Liza.  They probably already know all these words and use 'em when we aren't around anyway."
    l "If they know any of these words, I can assure you it is your doing.  No one else around them talks like this."
    su "Fine fine.  I'll watch my tongue."
    l "You've told me that for years now.  You don't really seem to be chaning what you say all that much."
    su "Ugh.  'Kay.  I mean it this time."
    l "Yeah, we'll see about that, now won't we?  Oskar, put down that tree!"
    su "Heh.  That's our boy.  A green thumb unlike anyone I know."
    l "Just because he likes uprooting plants doesn't mean his interests are in gardening."
    o "Sorry momma.  It was an accident."
    m "It totally was.  That tree has been loose for a while now."
    su "Uh oh.  You better watch out.  They are starting to be partners in crime."
    l "I worry for the safety of the entire village."
    db "NO!  WHAT IS GOING ON OUT HERE!  BAKA, COME LOOK AT YOUR SON!"
    ma "What are you-oh no.  Son, come here please."
    m "Aw, but we were just playing.  He didn't mean to remove the tree.  Honest."
    db "That is not what I am talking about!  Baka, control your little brat or I shall be forced to!"
    su "Relax old man.  He was just playing."
    db "I do not care if this runt is not my real son.  I will not have himself associating with trash."
    su "...excuse me?  What are you talking about?"
    db "Your pathetic excuse for a fake son.  That dirty little Majin you should have let die."  
    db "I have bit my tongue about him because you are my daughter.  But that does not mean you can befoul the mind of the brat before he can form his own opinions."
    su "...the fuck?  You fuckin' sad excuse for a father.  You don't listen to him, bro.  Come around whenever ya want."
    ma "No."
    su "What?  Ma, you don't gotta be afraid of-"
    ma "No.  I do not want my son dirtying himself by playing with lesser creatures.  He is too pure for that."
    db "And for once Bake, you actually speak some sense."
    su "What?  Do you hear yourself?  This is pathetic."
    o "What is wrong momma?"
    ma "I cannot stop you from associating with filth.  I did not raise you well enough, I guess.  For that I am truly sorry."
    m "Please let me play with Oskar, mom!  We were having fun."
    su "...you know, fuck this."
    l "Susanoo, no-"
    su "Susanoo is a fucking shitty name that has dirtied me for long enough."
    db "How dare-"
    su "Silence!  For now on, refer to me as Susa."
    ma "Susanoo, you don't know what you are saying!  This is not like-"
    su "No.  I've wanted to say this for years.  I'm sick of you, Iza.  I'm fucking sick of your behavior, how you treat people, how you-"
    db "Daughter-"
    su " I AM NO DAUGHTER OF YOURS.  I AM NOT PART OF THIS FAMILY, ANY LONGER!"
    db "Fine, farewell.  Traitor.  Befoul this home no longer."
    su "Traitor?  Ha.  What an amusing way of describing someone who isn't a fuckin' sadist."
    db "I don't know how else to describe an individual that turns her back on her people to go out and protect lesser creatures."
    su "Fuuuuuuuuck.  Yup, I'm against the murder and torture of creatures with higher intelligence.  Guess that makes me a monster."
    l "Oskar, get back to the house, now."
    o "But momma-"
    l "No, come with me now.  We will wait for Susa there, alright.  Say goodbye to Susa's brother."
    o "Bye!  See ya tomorrow!"
    i "Not a chance, you little maggot.  Yes, go flee with your maggot mother."
    su "HOW DARE YOU."
     
    "Iza, not now.  Please...please calm yourself."
    
    db "You heard me.  You are a family of maggots, and you will never be anything more than that."
    su "That is fuckin' it.  I am fuckin' sick of you and everyone you fuckin' associate with.  This is fucking war, old man.  I will see everything you own burnt to the ground before my time is up."
    db "Keep talking like that and your time will be up sooner than you would think."
    ma "Iza, no!  Do not talk to our daughter that way in front of my son or-"
    #play sound thump"
    
    db "I HEARD YOU.  I will talk how I want.  This is my house and my yard and your pathetic little offspring will deal with me talking however I want.  It is just lucky I am keeping it alive."
    ma "You will not talk-"
    #play sound thump"
    
    db "SILENCE.  You pathetic creature.  And stop bleeding all over my floor.  I am afraid it will stain."
    m "Momma, no!  Momma, don't hurt her!"
    db "Come any closesr kid, and I'll kill her.  You stay away from me, you pathetic runt."
    ma "My son...my son deserves..."
    db "Death.  If I was a kinder man, I would just kill it.  Put it out of its misery.  But I shall let it suffer.  Something else will almost certainly kill it soon enough."
    su "You two really are perfect for each other.  I wish you both years of wedded bliss."
    m "Sister...sister, please!  You promised."
    su "Come with me brother.  You have outgrown these two.  We can give you a proper name and will raise you as one of our own."
    ma "No!  ...don't...don't leave me, son."
    m "Momma...."
    ma "Just go Susanoo.  I do not wish there to be trouble in the home of my son."
    su "What the hell is wrong with you!  Open your eyes!  Just leave; come with me!  We can get out of here!  Away from him!"
    ma "No.  No, we are safe here.  I like it here.  I want my son to grow up in this village."
    su "Didn't you hear him!  He's going to hurt him!  It'll be safer for him out of this fuckin' shit town!"
    ma "Language, dear.  And he won't lay a hand on him.  Not when he was specifically bread for his high percentage.  Like..."
    su "What?  Percentage?  Like what?"
    ma "..."
    su "Like what?!"
    db "Heh."
    su "...?"
    db "Go ahead and tell her."
    su "Tell me what?"
    ma "..."
    su "What are you supposed to tell me?"
    db "If you don't tell her, I will.  Your choice."
    ma "My son...was bread for his percentage.  For his high percentage relation to the champions."
    su "...oh.  So...that's why you stay with him."
    ma "Do not look at me like that.  It is our job to produce new champions.  That is why I had my son.  And...that is why we had you."
    su "Me?"
    db "Oh come now.  Surely you don't think I slept with my sister for love?  Disgusting.  We had a duty.  And you did too, before you abandoned it."
    su "Your...sister?"
    db "Hahaha.  Have you not figured it out?  How else could we ensure the high percentage relation?  Be aware that there was no love here.  Never was."
    su "So...that is why I have these powers?  Why I don't age at normal rates?"
    db "Yes.  You had the highest percentage relation of this most recent generation.  I was to reveal this to you when you completed your training.  Before you betrayed me."
    su "..."
    db "Now get out.  Befoul my house no longer."
    ma "Yes, please leave.  I fear your presence here is disturbing my son."
    m "Sister..."
    su "Fine.  But if you fucking lay a hand on her, just one more fucking hand, I will return.  And I will break every last goddamn bone in your body.  The same goes for him."
    db "...w-what?  You don't-"
    su "Just fuckin' try me.  I beg you to.  Farwell, mother.  I wish you luck in raising your son."
    m "Sister!  Sister, please don't leave me!  Stay with us, please!  I...I need you."
    su "...sorry brother.  This is the end for us.  I can stay here no longer.  I wish you luck with these two.  If you ever need anything, send word.  I will return.  You will always be welcomed into our house."
    m "Sister..."
    
label su14:
    ro "What...what was that?"
    l "Sh.  Quiet, Roman.  It is just the wind.  Lay back down and relax."
    ro "...ngh.  I-I think we should keep moving.  We can't stop now."
    l "Calm yourself Roman.  We have been traveling from 20 hours straight.  I have lead your masters in the other direction."  
    l "We are safe here.  But you need to rest.  We have a full day of travel tomorrow."
    ro "No...no, I can't.  We need to go.  I swear I'm not tired.  I can go!  Just-just....let's just go!"
    l "You are shaking.  You claim you are not tired, but your body betrays you.  Come here.  Sit with me."
    ro "I just can't go back.  I've seen what they do to the ones who try to escape."
    l "You are not going back.  Shh.  Shhh.  Everything is fine, Roman.  You will be safe with me.  I will let no harm fall upon you."
    ro "Thank you.  Thank you so much, Miss Liza.  You have been too kind to me."
    l "Nonsense.  It is you who are doing a favor to us.  The rescue needs strong Majin to help protect it."
    ro "I'm afraid I will not be much help.  I'm just a cook..."
    l "Oh, you are much more than that.  I saw you defend yourself while we were trying to escape.  I couldn't have gotten out of there without your help, Roman."
    ro "You seemed to have the situation under control."
    l "Afterwards, yes.  That is true.  But there were more of them than I thought, and I certainly would have been overwhelmed."
    ro "No..l didn't do anything.  I couldn't have saved myself if it came to it."
    l "But you did.  You already have.  You have been told for years that you are worthless, that you have no power...but they were lying to you, Roman."
    ro "I'm frightened, Miss Liza.  If they catch up to us-"
    l "Please, just call me Liza.  Miss Liza sounds so formal."
    ro "Sorry, Liza."
    l "You are a free Majin, Roman.  There is no need to make yourself subservient to anyone else."
    ro "...right.  Perhaps...perhaps I could do with a brief rest.  But we will leave as soon as we awake, right?"
    l "Of course.  Just rest for now."
    ro "Thanks...thanks again."
    
    ##put in scenes from Kazu's path here; all necessary birthday scenes must be inserted
    
label su15:
    su "Sorry for the wait.  Took me longer to get out of the party than I thought it would."
    l "Not a problem.  I was just looking around your office.  Different than I remember it."
    su "Eh.  Well, I moved some stuff around, I guess.  Damn desk broke a couple of years ago, so I had to get a new one."
    l "Took down all the old photos with it, it looks like."
    su "Er...they're still around.  Got 'em packed up over here.  Just...just couldn't keep on looking at them everyday."
    l "I understand.  Certainly, they would bring back some bad memories."
    su "Hrm?  No...no, they bring back good ones.  Ones of us.  And him.  I just...looking at them just made me feel...fuck, I dunno."
    l "Ah.  Well, yes, I suppose I can understand.  I look back on those days fondly as well."
    su "...right.  Ahem.  Well, what did you find out the other day?"
    l "I managed to recover a file from the Akemiya compound.  It appears they have been attempting to locate our son for some time now."
    su "Did it...say how he escaped?  I mean...I thought I saw him die."
    l "No, the files did not cover that.  Mostly they have just been tracking his movements.  For the greater portion of this past year, actually."
    su "I can't believe I couldn't fucking tell he was still alive.  This is all my fault."
    l "Susa, this is nobody's fault except the people who tried to hurt him.  It is them that drove him from us."
    su "But he's been alive this whole time!  He has been out there, lonely and frightened."
    su "He has needed help and I have left him alone!  Our son, Liza!  I should have protected him!  FUCK!"
    l "Calm yourself, Susa.  It is because of you he has managed to stay alive this whole time."
    su "Horse shit.  I didn't even look for him.  I left him there.  It is my fault he is not here with us.  It is my fault that we...ah, fuck it."
    l "And if you had not trained him, Susa?  What if you had not instructed him how to avoid detection?"  
    l "Or if you had neglected to show him how to live off the land?  The fact that he still breaths is a testament to your success, and not of your failure."
    su "..."
    l "And we will find him again.  He is out there.  We will rescue him, Susa.  Bring him back home."
    su "...shit.  Well, where is he?"
    l "That I am uncertain of.  He appears to be somewhere in the northern forests.  Like you have outlined on your map here."
    su "Hmph.  Lucky guess.  After you told me he was still alive, I tried to narrow down where he could be."  
    su "This is one of the few areas I haven't collected much information on, so I had assumed it was possible."
    l "Well, at this point it looks more than possible."
    su "Well, that's good enough for me.  When are we heading out?  I'm good to go right after the party.  Wait, fuck the party.  Roman will understand.  How's now for ya?"
    l "Heh.  I love the enthusiasm, but we are not ready yet.  The norther forests are quite large.  A lot of area to cover.  I need to narrow it down a bit further before we just search the whole thing."
    su "Fuck.  Makes sense, but still."
    l "It is only a matter of time now, Susa.  Try your best to be patient.  I know patience wasn't always your strongest virtue."
    su "Damn right it wasn't.  It...it has just been so long.  Since...since we were a family, ya know?  I...I dunno.  Fuck.  When will you be ready."
    l "I suspect the preparations will take another week."
    su "Sounds good.  I'll be ready within a week then.  Just let me know when you're ready to leave."
    l "Hm...perhaps, Susa, this would be a job best suited to me.  I am used to sneaking into places."  
    l "Another individual prowling the area would raise the chances we would get seen.  We could lead them straight to him."
    su "Well, then we'll just have to fucking wipe them all out, won't we?  I am not letting you do this by yourself."  
    su "You don't take me with you, I'll head there myself.  Now, what sounds best to you."
    l "Heh.  Well, we always did make a great team."
    su "Hell yeah we did.  Time for a little reunion, eh?"
    l "...alright.  I will keep you informed.  But for now, we better head back to the party.  I'm sure Roman will be excited to see the two of us."
    su "Sounds good to me.  But if Soume asks me to dance, I'm pointing him in your direction.  Poor guy can't control his arms.  Almost gave me a black eye last time."
    l "Eh...perhaps I'll just have some punch.  Let Roman show me around.  Anything to avoid dancing at all costs."
    su "Aw, you're no fun."
    
    ##Put in the Liza/Kazu scene here
label su16:
    
    "Put in something here, idek."
    

#put in Susa/Kazu chat scene here...?
    
label su18:
    #play sound knocking"
    su "Fuuuuuuuuuuck.  Kazu, I told ya already.  I'm kinda fuckin' busy right now.  Come back another time.  Or never.  Whatever.  Just leave me alone for now."
    l "Um...it is me, Liza.  Doctor Osamu is nowhere to be seen right now, Susa, I assure you."
    su "Liza?  Already?  Damn, you move fast.  We didn't even get off the phone that long ago."
    l "I told you I was already moving.  We needed to meet, quickly."
    su "Yeah, you said that.  I just didn't know quickly meant right fuckin' now.  How'd you even get in?"
    l "Soume was actually outside in the forest.  I think I frightened him.  He certainly didn't expect any one else to be out there.  Something about looking for new specimens for his garden."
    su "Yup, that sounds like Soume.  The guy never seems content with the number of plants he already has."  
    su "Can't complain though.  Makes the shrine look a hell of a lot better than it did when I first opened it."
    l "Hm.  Regardless, were you able to find the documents I asked you about."
    su "Ah, yes.  The location of the Akemiya compound in the north forest.  Took me a bit, but I knew I fuckin' had it somewhere.  Managed to find it while Kazu was berating me for something."
    l "Right.  Excellent.  Yes, this matches up with the intel I've collected.  Yes, he has to be there, Susa!  This is it.  I knew it!"
    su "Are you sure, Liza?  Positive, right?"
    l "Susa, I'm positive.  He is here.  And he is alive, Susa!  I've seen pictures of him.  Not recent, but older than when we last saw him."
    su "Oh Liza...this is amazing.  I...I don't even know what to say.  I'm just so...happy."
    l "I agree.  I almost couldn't believe it.  We will need to plan this carefully.  If this is where I think it is, he might still be under Akemiya control."
    su "We should go now.  I'll get my things and-"
    l "Susa, no!  We can't just storm out there without a plan.  There might be demon hunters.  We might be walking into a trap.  We can't just run out there without a plan, Susa."
    su "We can't leave him there!  We move now!  You're not gonna-"
    l "We won't do any good dead!  Relax.  Calm yourself.  He has been safe all these years.  A couple extra weeks is all I ask for.  Let me work out a plan first."
    su "...I don't know."
    l "Susa, please.  I can't do this without you.  I need you."
    su "Eh...alright.  I suppose.  I can wait.  But we need to begin planning, and now.  I feel like I have to do something."
    l "Excellent.  Glad you came around."
    su "Well, you've always managed to persuade me into doing just about anything."
    l "...right.  Now, before we do anything, we need to collect more information around the area."
    su "I could send some people from the rescue down to check the area out."
    l "No.  No offense, but I've seen your students.  They can be rather clumsy.  This is something I would prefer to do by myself."
    su "I can help ya out, if you could use an extra hand."
    l "Tempting, but still no.  I'll need you when we go to rescue him, but I can move more freely on my own.  Nothing against your powers, Susa, but I have a bit of an advantage."
    su "Well, I guess the abillity to transform into an any probably would be useful at some point when you're trying to sneak around in the forest."
    l "Oh, it certainly does.  It has made it remarkably simple to spy on the Akemiya house, and to move in and out at my whim."
    su "Hmph.  So you're saying I'd just get into your way during the recon?  I dunno...I'm telling ya, it might be better to have both of us there."
    l "No, Susa, I really must insist.  That will only increase our chances of being seen.  It is for the best if I do this alone.  My shapeshifting abilites will make the task quite simple solo."
    su "But what if something happens to you Liza?  We can't risk something bad happening fuckin' now.  We are so damn close."
    l "Right, I agree. Which is why we musn't become careless.  It will be better for me to do this alone.  Please, Susa.  Please just trust me."
    su "Ehhhhh...fine.  I guess.  But you better come back soon.  And in one fuckin' piece."
    l "Susa, you have nothing to worry about.  I have handled more capable foes than this."  
    l "I expect this area to be even less heavily guarded than their main compound.  Well, perhaps for a few scattered scientists, but that is to be expected."
    su "Yeah, I guess.  Wait, what?  Why would scientists be out in the middle of the forest?"
    l "Oh...well.  Hmph.  I am afraid I might have said more than I wish I had at this point."
    su "...meaning what?"
    l "Er...ahem.  Well, you see..."
    su "Meaning what, Liza?  What are you trying to  hide from me?  What don't you want me to know."
    l "It is...nothing serious.  I just...well, in one of the reports I managed to steal, it said that they were conducting experiments on Majin in that facility."
    su "Experiments?  What kind of experiments?  What did they do to our son?  I swear, if they harmed on fuckin' hair on his body I will personally rip their fuckin' throats out through their-"
    l "Susa!  Relax.  This is why I did not want to expound on this, especially when I have so little detail to go on."
    su "Well, what the fuck do ya know?  Is he alright?  What were they testing on him?"
    
    l "Susa, I don't know.  I don't even know if he was a specimen being tested on."  
    l "It never said that directly.  RIght now, I am working under the assumption that he is fine, and at worst he might be slightly altered physically by this experimentation."
    su "Fuck.  Fuck, Liza, I should come with.  We should go now, we need to-"
    l "No.  We stick to our original plan.  Give me to weeks to scout the area, please."
    su "We cannot waste our time like this!  Our son is in danger!  Oskar needs us, now!"
    l "No!  We cannot rush in and hope to help him in any meaningful way!  We need to know what we are dealing with."
    su "...hmph.  I dunno about this..."
    l "Please, Susa.  Two weeks.  Two more weeks is all I ask for.  And then we can move.  We can rescue him.  We can do it together, but I just need some time."
    su "Sigh.  Fine.  Two weeks.  He'll be safe for another two weeks.  Right?  Right.  He'll be fine..."
    l "Good.  I'm glad you understand.  Trust me, I want to rescue him as much as you do.  But this is no time to abandon all self restraint."
    su "Fine, yeah.  I get it.  So, what exactly is our plan.  We need to go over your scouting work in detail.  This has to happen perfectly."
    
label su19:
    su "Hmm...yeah.  Yeah, I guess that sounds about right."
    l "Susa?  Do you still have reservations?  You do not sound fully committed."
    su "Huh?  Oh...uh, nah.  Just the same stuff, y'know.  Waiting.  Leaving our son in their hands any longer than he has too.  Just makes me nervous, is all."
    l "Mm.  I understand your concern, but I assure you, this is the most efficacious way of freeing him.  We risk his safetly if they know too much of our plan."
    su "I know.  Doesn't mean I have to like it any."
    l "Yes, well...I am sure you will like it more when Oskar is back with us."
    su "Yeah, that'd probably fix a lot of things."
    l "Hm?  What are you referring to."
    su "Well...nothing.  Just, y'know.  It could fix things."
    l "...like us?"
    su "Ah.  Er...yeah.  Like us, for instance.  Like, if we could just go back to before this all happened.  Just the three of us.  I think...I think things might be different."
    l "I...well, we can discuss that later.  We musn't allow that to distract us now."
    su "Don't tell me it hasn't crossed your mind, Liza.  What we could be again, if we had Oskar back."  
    su "I know that what happened to Oskar...losing him...certainly put a strain on us.  On our relationship."
    l "That, Susa, was a long time ago.  A lifetime ago.  Things have changed.  For now, we should focus on Oskar.  He needs us."
    su "Yeah, well...I need us too."
    l "..."
    su "Liza, think back.  Remember.  Remember what we were."  
    su "I...I don't think I was ever happier.  Those first nights together...they were the first time I was ever truly happy.  Don't tell me you don't feel the same."
    l "...I cannot deny what I felt."
    #***
    su "You were absolutely fuckin' amazing today.  I've never seen any one round up so many Majin."
    l "It was nothing.  You just need to be able to round them up and the rest is rather easy."
    su "Yeah, well, still.  Try telling that to the rest of the demon hunters.  Any more than two Majin and they're fucking lost.  Just running around and they end up failing to get even one."
    l "I appreciate the recognition.  Shall I return to my room now?"
    su "Nah, stick around for a bit.  Have a drink.  That is, if ya want to.  Don't want to keep you from anything."
    l "Oh...well, sure.  I suppose I have time for a drink or two.  In celebration, of course."
    su "Now you're talking.  Let me see what we have hear...suppose I could break out the good stuff.  Shit is strong though, just to warn you."
    l "Hmm..."
    l "Not bad.  I've had stronger though.  This stuff is pretty good though."
    su "Ha ha!  Well, look at you.  Y'know, if I didn't know better, I wouldn't be able to even tell you're Majin."
    l "Hm?  Sorry, what is that supposed to mean."
    su "Huh?  Oh...you know.  Nothing person, I don't care what you are, as long as you can get the job done.  People just say things, you know."
    l "Ah.  Well, good to know you're open minded like that.  I suppose."
    su "Mm hmm.  Well, my family wouldn't be happy if they knew I thought that, but I've always thought they were a bit messed up to begin with.  You're just...amazing though."
    l "Heh. Susanoo, I am honored by your praise.  I hope to some day equal your prowress."
    su "That...uh, yeah.  I just-"
    su "Mm."
    l "S-Susanoo!"
    su "Sorry.  I just..."
    l "No.  Don't apologize.  Come closer."
    su "Mm.  Ohhh.  Come with me.  There is more privacy in my quarters."
    l "Of course...Susanoo."
    #***
    su "We were in love. I felt invincible."
    l "We were young.  We've both grown since then.  We can evaluate our relationship later.  For now, the only thing that matters is Oskar."
    su "I know that...I just thought...y'know.  Who knows what could happen to us from here."
    l "...I hate to ruin any fantasies of yours, but I wouldn't hope for something that might not ever happen again."
    su "...but we were in love once.  You know that to be true.  It...it could happen again."
    l "Once.  Long ago.  That's over now though, Susa."
    su "But-"
    l "I don't want to talk about this now.  Focus on your part of t'he plan Susa.  I will be back soon.  We can discuss details then. Details of the rescue."
    su "...right.  Sorry if I tried to push anything.  Oskar.  We should focus on Oskar."
    l "Yes.  I'm glad you've come to your senses.  I will be in touch, Susa."
    ###insert scene from Kazu path with Susa telling Soume/Riku she is leaving
    
label su20:
    m "Hmmm...and are you sure of this, Naomi."
    na "Aw, c'mon bossman.  Give me a bit more credit than that.  Course I'm fuckin' sure.  Heard it from the source directly."
    m "Ohhhh...this is too good.  This is perfect.  Looks like the time has finally come, dear sister."
    n "About time, too!  We've been waiting on this forevvvvvvver."
    na "Fuck yeah we have.  Didn't think we were ever gonna move on this.  Are you sure this time?  Like, positive?"
    m "Heh.  I had told you two that we couldn't just rush into this.  That at some time the opportunity would present itself to us."
    m "And now here we are.  What is that old maxim?  'Good things come to those who wait,' I believe."
    na "Yeah, yeah.  Well, we've been waiting for a fuckin' while now.  You never told me how long we were gonna be waiting for this shit to actually happen."
    m "Oh, come now, Naomi.  When did I ever say I was going to tell you everything?  These things take time, you know."
    n "Yeah, Naomi.  Nii-chan here is the boss.  So how about you sit down and keep quiet.  'Kay?"
    na "Hey!  You little fuckin' twerp.  This is a grown-up conversation anyway, so how about you take your little toys and run out to the park. 'KAY?"
    m "Now, now.  Naomi, Norah, this is no way to talk to each other.  This is a time for celebration."  
    m "These petty bickerings can wait for a more opportune occasion.  One where we do not have so much planning to discuss."
    na "Ugh.  Fine.  So, I was thinking, you know, about how we could-"
    m "Oh, Naomi.  I am afraid you misunderstood me.  Again.  We are not going to actually discuss strategy.  I am going to tell you what we shall do."
    n "Nii-chan has had this planned out for years already!  We don't need any of your ideas!"
    m "Precisely, Norah.  Come now, Naomi.  Did you really think I wasn't already three steps ahead of those pathetic little Majin.  I hope you hold me in higher esteem than that."
    na "Well, what the fuck are we supposed to be talkin' about then?  You already told me what I was going to do, like four fucking times.  What else is there to talk about?"
    m "I just need to confirm that you can hold down things on your end.  You understand?  I need guarantees that you will not fail me."
    na "'Course not, bossman.  I ever let you down before?  I'm not a girl that lets a perfectly good meal get away."
    m "Good.  I'm glad to hear it Naomi.  I have just been a bit worried about your...abilities as of late."
    na "Huh?  The fuck is that supposed to mean?  My 'abilities' are just as damn good as they have always been.  You know that."
    n "Oh.  Tut tut, Naomi.  Nii-Chan wasn't too happy you let the foxy and all his friends get away last time.  Isn't that right, Nii-chan?"
    m "Ha!  Oh, Norah!  You are as perceptive as always!  Yes, Naomi.  You...disappointed me last time.  I thought we were clear on you distracting Soume while I took care of what needed taken care of."
    na "You changed the fucking plans on me halfway through!  You can't expect me to know what you're gonna do unless you tell me to!"
    m "Hm.  See, I was under the impression that I told you to hold off Soume.  That part of the plan didn't change, Naomi.  You needed to hold off Soume.  And did you?"
    n "Yeah, did ya, Naomi?"
    na "Quiet, you!"
    m "Oh, is that any way to talk to my dear sister.  It certainly is not her fault you cannot follow simple orders."
    na "Look, I...I was, but he got away.  I tried to drive him away from you, well, the direction I thought you were in, but-"
    m "But you drove him towards me instead?  Because I switched the target."
    na "Uh...yeah.  Yeah, that is what I was going to say."
    m "See, I thought you were a capable fighter, Naomi.  I did not want you to try and chase him."  
    m "If I wanted I dog, I would have just bought one.  I wanted a warrior.  And you could not even fight one little Majin long enough to let me finish what I had started."
    na "I...well, you see..."
    m "No matter now, Naomi.  You can make it up to me.  I am the forgiving kind, you see.  I will let this one little failure on your part go unnoticed."
    na "Right.  Uh...thanks.  Won't happen again, bossman."
    m "Certainly.  But, I warn you, do not try my patience again.  I have a terrible habit of losing my temper if things continue to not go my way."
    n "Sure does!  I still remember what happened to the last guy!"
    n "...ya know, I'm not sure if we ever did find his arm, did we Nii-chan?"
    na "..."
    m "Hmph.  That is all, Naomi.  I leave you to handle your end.  Take as many men as you need.  And have them ready, soon."
    na "Uh...sure thing.  I'll...take care of it, bossman."
    
    #play sound footsteps"
    
    n "I think you scared her, Nii-chan."
    m "She needs a bit more discipline.  Hopefully she can learn some before it is too late."
    n "Heh.  This is a naughty little trap you planned.  Think it'll work?"
    m "Oh, Norah, don't sell yourself short.  You were just as important in coming up with it as anyone."
    n "Aw, thanks.  Just glad to help out in any way I can, you know.  Can't let you get away doing all the fun stuff."
    m "Now, come Norah.  We must still discuss our role in these coming events.  I would hate to leave any thing to chance."
    
    
label su21:
    su "Hmm...I think that is everything."
    l "I hope there is nothing else.  We need to pack lightly.  Considering the security I observed there, we need to be able to move quickly if the need arises."
    su "I'm only packing the essentials.  Medicinal shit and what not.  Just in case, ya know?"
    l "Hmm...I suppose that is acceptable.  Do you have the map?"
    su "Of course I have the map.  Been keeping it in my pocket for the last week now.  Tracing the path we're going to take."
    su "...to get our son back.  Probably have the fucking thing memorized by now, but yeah, I got it."
    l "Oskar will be fine.  I overheard some guards talking and I know he is there."
    su "So, how many guards are we going to be facing?  Need to know how many of these assholes are standing between me and our son."
    l "Well...it is not that simple.  It is not the guards we need to worry about.  There are other...precuations in place that will prove more troublesome."
    su "Oh yeah, like what?"
    l "It is somewhat difficult to explain, but they must have someone on their side that is adapt at altering perception."
    su "...meaning what?  That doesn't make fucking sense to me."
    l "Well, the path to the complex itself is fairly straightforward.  A couple of minor hills, but relatively uneventful."
    su "Yeah, so?  Sounds good to me."
    l "The problem is that the true path has been disguised.  There are numerous other paths meant to get us lost.  If we lose our way, we could be lost for days.  We not be able to find our way out."
    su "Ngh.  They couldn't make this easy for us, could they?  Fuck, and how are we supposed to get through?"
    l "I managed to acquire the proper path from a guard.  Disguised myself and followed him to the compound.  I should be able to find my way back."
    su "Excellent.  I can't believe the Akemiya are so fucking stupid that they haven't realized-"
    
    #play sound knocking"
    
    l "Expecting someone?"
    su "Yeah, I think it is just Soume."
    su "Come in!  Door is unlocked."
    s "Ah.  Miss Susa...Miss Liza.  Sorry to interrupt."
    su "No worries.  We were just about to leave.  I just wanted to give you the keys to my office.  Feel free to use it while I'm gone."
    s "I...I don't know if this is the best idea."
    l "I agree with Soume.  Why aren't you leaving Doctor Osamu in charge?"
    su "Ugh.  That guy has lost his fucking mind, that's why.  No, Soume is the best person for the job.  Here, Soume."
    s "I...perhaps you should-"
    su "I insist!  Take the damn keys, already."
    s "...very well.  As you wish, Miss Susa~!"
    su "Great.  Thanks again Soume.  I won't be gone longer than a week.  Just keep things running smoothly."
    s "Thank you, Miss Susa."
    su "Well, I guess we should be-"
    s "Miss Susa?"
    su "Hm?  What is it Soume?"
    s "...I mean it.  Thank you.  I...I can't tell you how much I appreciate it."
    su "Huh?  Why are you crying?  Calm down.  I'll be back soon.  I'll be safe, promise."
    s "...right.  I just...well, goodbye.  Miss Susa."
    su "Yep.  Bye.  Coming, Liza?"
    l "Hm?  Uh, yeah.  I will be right behind you."
    
    #play sound door closing"
    
    l "Odd behavior from him, don't you think?"
    su "Nah, Soume has always been a bit sentimental.  I'm sure he just isn't used to being the one in charge.  Lots of responsibility."
    l "And you trust him?  Doctor Osamu has been getting more desperate."
    su "Yeah, well, like I said.  That guy is out of his damn mind.  Come on, we have a lot of ground to cover."
    l "Sure.  Once we leave the rescue, follow me.  I already know the quickest way there."
    su "Heh.  Of course you would."

label su22:
    su "According to the map, we're close.  We should be able to see it soon."
    l "Well, we could, but like I have said it is hidden."
    su "Ngh.  Fuck, just like the little twerp to make it hard on us."
    su "And where is our son?  Where in the complex is he?"
    l "I could not enter the complex last time I was here.  Too many men.  Even with my talents, it was too dangerous."
    su "I see.  Well, damn, this might take a while then.  How many buildings in total."
    l "From what I could see, five.  Each one of them had multiple floors, and I heard one of the guards speak of an underground facility."
    su "Underground facility?  Why the fuck would they need one of those?  This place is already hidden as it is."
    l "From what I could make out, there is experimentation going on down there.  Something they do not want the rank and file soldiers to oversee."
    su "Hmph.  If anything has happened to Oskar, I will decorate the walls with a fresh coat of their blood."
    l "And I will join you."
    su "I can't even begin to think of what Oskar must be feeling.  I...I hope he doesn't think we abandoned him."
    l "That...I'm sure he doesn't think that."
    su "It has been years, Liza.  Years, without seeing us, without hearing from us, without even knowing we were thinking about him."
    l "...he knows we love him.  He's known that.  Always."
    su "Yes, but how-"
    l "Susa, stop right there!"
    su "Huh?  What's the matter?  Why are you freaking out."
    l "This is the edge of the compound.  From here on out, we must follow a specific path or we will not be able to proceed."
    su "Ah.  Well, then, what do we need to do."
    l "I need you to follow me.  Stay close.  A single wrong turn and we might not find our way out."
    su "Gotcha."
    ###insert minigame here.  Liza must navigate the woods to the destination

label su23a:
    su "This...this all is looking familiar."
    l "We...we should have been out by now.  I don't know what happened."
    su "Perhaps we took a wrong turn somewhere?"
    l "No.  No, that can't be.  I followed the map exactly!"
    su "Are we lost?  Liza, please tell me we aren't lost."
    l "I don't...this isn't right.  No, something is definitely wrong.  The complex should be right ahead."
    su "Perhaps past these trees?"
    l "No, no there shouldn't be trees here.  Oh...fuck.  I must have read the map wrong.  I messed up.  I'm...I'm so sorry, Susa."
    su "Well, we can just turn around?  Right?  We'll just head on back and change our path.  We'll just be a little late."
    l "It isn't...that easy.  This was designed to keep intruders out."
    su "So?  Hasn't stopped us yet."
    l "The illusion will keep changing now.  We might find our way out by luck, but if we were able to find our way out by just luck...I estimate it would take us several years."
    su "What?  Oh, Liza, please tell me you're joking.   We're so fucking close."
    l "I...I'm sorry Susa.  I've failed us.  I failed our son."
    su "Well, c'mon.  Get up.  We'll find our way out."
    l "Oskar...I'm so sorry."
    su "C'mon.  Liza, don't quit.  Get up!"
    l "...I'm so sorry."
    
label su23b:
    su "Ngh.  That...that took longer than I thought."
    l "Yeah.  That is all my fault.  If I had not taken those incorrect turns, we would have been here long ago."
    su "Eh.  Not a problem.  I'm just a bit tired is all.  I need to...ngh...rest for a moment.  Fuck,  I think I twisted my ankle."
    l "Wait here a moment.  I want to check the perimeter."
    su "No, I can come with.  Just let me-"
    l "Susa, please.  You need to be rested for what is to come.  I expect heavy resistance once inside.  Just let me check the outside on my own, please."
    su "I...I don't know if that is a good idea."
    l "Of course it is.  I shall return shortly.  Nearly immediately.  Please wait for my return, right here."
    su "Ngh.  Fine.  I'll stay put.  I'll be good, and all that fucking nonsense."
    l "Heh.  Excellent.  I shall return at once."
    
    #play sound scurrying
    
    su "Ngh.  Fuck.  Stuck here like some useless sod.  What a fucking time for me to turn my ankle."
    su "Still..."
    su "This doesn't seem right."
    su "This compound was heavily guarded last time, Liza said."
    su "...but I don't fucking' see anyone.  Where the hell are all the Demon Hunters?"
    su "This...is too quiet.  Like...like we walked into a-"
    l "AHHHHHHHHHHHHH!"
    su "Liza?  Fuck, Liza!"
    l "NOOOOOOO!"
    l "NGH!"
    su "Liza!"
    
    #play sound footsteps
    
    su "Liza!  Liza, you're bleeding!  What is that, over there?  What attacked you?"
    l "Ngh...that is...Oskar."
    su "Oskar?  Oskar?  But...but how?"
    l "He...he was made feral, Susa.  He...he has been like that for a while...it...ngh...looked like."
    su "No...Oskar.  No.  No, this can't be happening."
    l "Susa...please kill me.  Now."
    su "What?  Not, this wasn't your fault!  Liza, no come with me!  We...we can talk about this-"
    l "No!  Now!  This is not a request.  I would do it myself, but my arm..."
    su "I...we can fix this.  Kazu can fix this.  This is nothing."
    l "He...bit me, Susa.  It is only a matter of time now."
    su "...no.  No, we can do something."
    l "No.  I will be feral soon.  Please...please don't let that happen to me.  Just kill me.  I...I don't want to become like him.  Like Oskar."
    su "Liza..."
    l "Please...Susa.  Please."
    su "Liza.  I...I love you."
    l "Ngh.  NGH."
    su "Liza?  LIZA?"
    
    #play sound cutting"
    
    l "...urk."
    su "Liza...I'm so sorry.  Liza..."
    l "..."
    su "I'm so sorry."
    
label su23c:
    su "Holy shit.  This place really did just appear right the fuck out of nowhere."
    l "Heh.  Yes, they did manage to hide it quite well, didn't they.  I must admit, I was taken by surprise the first time I saw it as well."
    su "Mm.  Well, at least we're finally here.  I got a bit nervous there for a while, but you knew where you were going."
    l "I told you that you could trust my sense of direction.  Having traveled the path once before, it was unlikely I would forget it anytime soon."
    su "Hm.  Well, lets get in there and break Oskar out.  I'm sure he'll be anxious to see us.  Can't imagine the hospitality is too good here."
    l "Yes.  Well, first let me check the perimeter.  We need to see exactly what we will be dealing with."
    su "Sounds good.  I'm right behind you."
    l "No, you stay here.  It will be quicker if I go by myself."
    su "And if you run into trouble?  Nah, I'm coming with.  Arguing with me will just waste time.  C'mon.  I'll lead."
    l "Ugh.  You have always been terrible at taking order, do you know that?"
    su "Yup.  Much better at giving them.  Never did learn how to follow them that well, did I?"
    l "I should say not."
    su "...quiet out here, don't ya think?"
    l "Hm?"
    su "Well, you said there were a lot of Demon Hunters last time you came, right?  That you followed one of them up here?"
    l "Yeah?  So?"
    su "Well, where are they?  Don't see any of them around.  Figured we'd run into a couple of them by now."
    l "...that is true.  I...I don't know what to say.  This is odd."
    su "Be wary.  I don't think-"
    u "RRRR"
    su "Liza watch out!"
    l "Ngh!"
    su "Huff...you alright?"
    l "Yeah....thanks to you."
    su "Oi, ya fucking mongrel.  What the fuck is wrong with you?"
    u "RRR."
    su "What...is that thing?"
    l "...oh no."
    su "Disgusting.  Yeah, I'm talking to you.  Stop looking at me like that."
    l "...it is...a rabid Majin."
    su "Eh?  Rabid?"
    l "...Oskar."
    su "Huh?  What is that?"
    l "...this is Oskar.  I...I know his face."
    su "Oskar?  ...no."
    su "...no.  No.  NO!  This...this can't be!"
    u "RRRR."
    su "Oskar!  Oskar!  Son?  Remember me?  Susa?  And Liza...we're here for you."
    l "...it isn't any use Susa.  He...he has gone rabid.  There is no doubt in my mind.  I've...seen this before."
    su "Well...we can fix him.  We'll cure him, I'm sure Doctor Osamu knows..."
    l "There is no way to bring one back after they have gone rabid, Susa.  You know that."
    su "But...but maybe we could..."
    l "No.  No, there is nothing we can do.  But put him out of his misery."
    su "What?  THAT IS OUR SON, LIZA!"
    l "I know!  I know that, Susa!  Which is why we have to do it.  Our...boy.  He...he can't live like this."
    l "This isn't a life, Susa!  He is a slave to his hunger!  He...he doesn't think.  Doesn't feel.  Please."
    su "...no."
    l "Please help me, Susa.  I...I can't do this alone."
    su "...Oskar."
    l "Please."
    su "...this is all my fault, Oskar."
    l "..."
    su "I...I let you down.  I'm a fucking failure as a mother.  I couldn't protect you..."
    l "Susa."
    su "Fine.  I'll help you."
    
    ###insert battle Susa and Liza vs Oskar
    
    l "Ngh."
    su "Oh no...what have we done."
    su "How could we have let this happen."
    l "I...I don't...I'm going to be sick."
    su "Why?  WHY?  NOT NOW!  WHY DID THIS HAPPEN NOW!"
    m "Hmm...perhaps because I wanted it too."
    su "...you."
    m "Me!  Yes, I'm so sorry, you two.  Nothing I could do, really."
    su "You fucking piece of shit.  You little fucking-"
    m "Oh, come now.  Such vulgarity.  And in front of your child too!  Although...I guess he can't hear you right now, anyway."
    l "How could you do this to him?!"
    m "Me?  Oh, my, what vulgar accusations.  I would not do this to any one.  I resent your accusations."
    m "I mean...if one in his condition was to just fall into my lap, then certainly I would use that opportunity, you know?"
    m "But, alas, poor itty bitty Oskar was like this when we found him."  
    m "The person at fault for this is...well, you two.  Hmm...persons at fault would have sounded better.  Ah well.  Hindsight, you know."
    su "You pathetic little shit.  You come fucking down here and I will rip the skin from your body."
    m "Susa, darling, you offend me!  I haven't seen you for so long, and this is how you talk to me.  Such foul language.  Is she like this at home, Liza?  I can only imagine."
    l "Come and face us coward!"
    m "Mm...I'd prefer not too.  The view is much nicer up here.  The sun is setting, and you can see it just perfectly above the tree line."
    su "When I get my hands on you, I'll-"
    m "What?  What exactly will you do?  Bleed everywhere?" 
    m "Come now, Susa, don't act like I am not far more powerful than you could dream to be.  I eat properly, after all.  You're looking a bit thin.  A bit underfed."
    m "Starving yourself, are you?  What a shame.  You look awful.  Tut tut."
    m "Anyway, I must be going."
    su "NO!  COME DOWN!  NOW!"
    m "Hmm...I will politely decline."
    m "I could, you know.  Come down there.  Kill the both of you.  It would be rather enjoyable, I dare say.  Liza is looking absolutely delicious today, by the way."
    m "But...but I think I would prefer it if you two were alive."
    l "And why is that."
    m "Hmm...I don't want to ruin the surprise.  But, ask yourselves this."
    m "I've been watching up here.  I knew you would show up.  I've had the jump on you.  I could have easily killed you, if I wanted to."
    m "And yet...here you are.  Here you stand.  Now...why would I let you two live?"  
    m "Perhaps...perhaps living would be worse?  Perhaps once you find out what happened, you would wish for death.  And you know how I am loathe to give you what you desire."
    su "...you."
    m "Ah yes.  The lightbulb has finally been lit!  Still rather dim, but at least it is something."
    m "That's right.  I haven't killed you because it is unnecessary."
    m "I've already won."
    m "Well, I shall see you later.  I am in a bit of a hurry."
    su "Liza, the resuce!"
    l "Right.  I'll follow you there."

label su24:
    su "Liza, how are-"
    l "Don't.  Susa, stay back."
    su "I need to see my-"
    l "Susa, stay here!  You don't want to see-"
    su "OUT OF MY WAY!"
    l "Ngh."
    su "Soume?  Roman?  I...no."
    l "...I'm so sorry Susa.  He must have been waiting for you to leave."
    su "Destroyed.  All of it.  How long have you been here."
    l "Couple of hours before you.  I can move quickly in some of my forms."
    su "Where did you put the survivors?"
    l "..."
    su "Liza, where are the students?"
    l "...oh Susa."
    su "Where, Liza?"
    l "Susa, sit.  It'll be alright."
    su "No."
    su "No.  Why aren't you looking?  What is wrong with you?  They are under this rubble!  They need our help!  WHY AREN'T YOU LOOKING?"
    l "Susa, please relax.  You...you can't move that.  Sit."
    su "No, they're under here!  I know they are.  I can help them.  I can make this right.  Get Kazu, now.  I need Kazu."
    l "...Kazutaka is dead, Susa."
    su "No.  -sobbing- No.  No, he's not."
    l "He is.  I found him in his lab."
    su "No.  No.  Get Soume.  Soume can heal him."
    l "Susa, please...I want you to sit down."
    su "Get me Soume, Liza!"
    l "..."
    su "I'll look here.  You get Soume."
    l "When...when I found Doctor Osamu, he was impaled."
    su "Go get Soume, Liza."
    l "He...he was impaled by a plant.  A large vine.  One of Soume's.  I'm certain."
    su "-sobbing-"
    l "Susa, please sit."
    su "This...this can't be happening.  First Oskar, now this.  This?  How could I be so blind?  What the fuck is wrong with me?"
    l "Susa this isn't-"
    l "Wait, move that back!"
    su "Hm?"
    l "Who is that?"
    su "...Riku?  Riku!  Is he...oh.  He...he isn't..."
    l "I hear a pulse!"
    su "Riku!"
    l "It is weak, Susa, but it is definitely there.  He's alive.  For now."
    su "...oh thank goodness.  Fuck."
    l "Susa, leave with Riku, now."
    su "But, I need to look for-"
    l "No, it isn't safe for him here.  Take him to where we first met.  Hide him.  Mamoru will be looking for him."
    su "..."
    l "I'll stay here.  I'll look for others.  I...I haven't found Roman yet.  He might be alive still.  I need to find him before I can leave."
    l "Take Riku.  Run, Susa.  I will meet up with you later.  I promise."
    su "Please...be safe.  I could not stand to lose you too today."
    l "I will.  Now go."
    su "I'm so sorry, Riku.  I've let you down.  I've let everyone down."
    su "I've let him win.  I can't believe I let him win."
